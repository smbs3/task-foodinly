import React, { createContext, useState } from "react";
import useAxios from "axios-hooks";
export const FoodContext = createContext();

const FoodProvider = ({ children }) => {
  const [, refetch] = useAxios("/cart", { manual: true });
  const [cart, setCart] = useState(null);

  const createCart = () =>
    refetch({
      method: "POST",
      data: {
        products: [],
      },
    });

  const addItemCart = (cartId, productId) =>
    refetch({
      method: "POST",
      url: "/cart/add-product",
      data: {
        cartId,
        productId,
      },
    });

  const getCart = () => {
    return refetch({
      method: "GET",
      url: `/cart/${cart?.id}`,
    });
  };


  const addProduct = async (productId) => {
    if (!cart) {
      const cartCreated = await createCart();
      setCart(cartCreated.data);
      return addItemCart(cartCreated.data.id, productId);
    }
    return addItemCart(cart.id, productId);
  };

  return (
    <FoodContext.Provider value={{ addProduct, getCart }}>
      {children}
    </FoodContext.Provider>
  );
};

export default FoodProvider;
