import ReactDOM from "react-dom/client";
import './scss/main.scss'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./components/Layout";
import Home from "./pages/Home";
import Contact from "./pages/Contact";
import Axios from "axios";
import { configure } from "axios-hooks";
import FoodProvider from "./context/FoodContext";

export default function App() {

  const axios = Axios.create({
    baseURL: process.env.REACT_APP_API_URL,
    headers: {
      authorization: `Bearer ${process.env.REACT_APP_API_TOKEN}`
    }
  })
  
  configure({ axios })

  return (
    <FoodProvider>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="contact" element={<Contact />} />
        </Route>
      </Routes>
    </BrowserRouter>
    </FoodProvider>
  );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);
