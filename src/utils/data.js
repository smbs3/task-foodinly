export const blogItem = [
  {
    image: "https://foodingly.netlify.app/assets/img/blogs/blog-1.png",
    category: "Food and water",
    title: "Healthy food and nutrition among all the children",
    text: "Lorem ipsum dolor sit amet, consectetur notted adipisicing elit sed do eiusmod...",
    date: "20 Dec, 2021",
    by: "Admin",
  },
  {
    image: "https://foodingly.netlify.app/assets/img/blogs/blog-2.png",
    category: "Delicious",
    title: "How to make delicious and spicy chicken grill",
    text: "Lorem ipsum dolor sit amet, consectetur notted adipisicing elit sed do eiusmod...",
    date: "20 Dec, 2021",
    by: "Admin",
  },
  {
    image: "https://foodingly.netlify.app/assets/img/blogs/blog-3.png",
    category: "Food and nutrition",
    title: "Historical culture of asian food and nutrition",
    text: "Lorem ipsum dolor sit amet, consectetur notted adipisicing elit sed do eiusmod...",
    date: "20 Dec, 2021",
    by: "Admin",
  },
];
export const ChefInfo = [
  {
    image: "https://foodingly.netlify.app/assets/img/chefs/chefs-1.png",
    name: "Jane Cooper",
    position: "Chief chef",
  },
  {
    image: "https://foodingly.netlify.app/assets/img/chefs/chefs-2.png",
    name: "Cameron Willamson",
    position: "Asst. Chef",
  },
  {
    image: "https://foodingly.netlify.app/assets/img/chefs/chefs-3.png",
    name: "Brooklyn Simmons",
    position: "Asst. Chef",
  },
  {
    image: "https://foodingly.netlify.app/assets/img/chefs/chefs-4.png",
    name: "Kristin Watson",
    position: "Nutricionist",
  },
];

export const productInfo = [
  {
    icono: "https://foodingly.netlify.app/assets/img/service/1.png",
    title: "Breakfast",
    text: "Eu occaecat mollit tempor denim consectetur. Labore labore est du commodo veniam.",
    image: "https://foodingly.netlify.app/assets/img/service/service-1.png",
  },
  {
    icono: "https://foodingly.netlify.app/assets/img/service/2.png",
    title: "Lunch",
    text: "Eu occaecat mollit tempor denim consectetur. Labore labore est du commodo veniam.",
    image: "https://foodingly.netlify.app/assets/img/service/service-2.png",
  },
  {
    icono: "https://foodingly.netlify.app/assets/img/service/3.png",
    title: "Dinner",
    text: "Eu occaecat mollit tempor denim consectetur. Labore labore est du commodo veniam.",
    image: "https://foodingly.netlify.app/assets/img/service/service-3.png",
  },
  {
    icono: "https://foodingly.netlify.app/assets/img/service/4.png",
    title: "Snacks",
    text: "Eu occaecat mollit tempor denim consectetur. Labore labore est du commodo veniam.",
    image: "https://foodingly.netlify.app/assets/img/service/service-4.png",
  },
];

export const opinions = [
  {
    date: "05 Jan, 2022",
    name: "Manresh Chandra",
    image: "https://foodingly.netlify.app/assets/img/review/review1.png",
  },
  {
    date: "08 June, 2022",
    name: "Santa mariam",
    image: "https://foodingly.netlify.app/assets/img/review/review2.png",
  },
  {
    date: "23 Feb, 2022",
    name: "Jack cremer",
    image: "https://foodingly.netlify.app/assets/img/review/review3.png",
  },
  {
    date: "23 Feb, 2022",
    name: "Mandeep harshaal",
    image: "https://foodingly.netlify.app/assets/img/review/review4.png",
  },
];

export const offices = [
  {
    title: "Head office",
    location: "New Mexico",
    direction: "4140 Parker Rd. Allentown, New Mexico 31134",
  },
  {
    title: "Washington office",
    location: "Washington",
    direction: "4517 Washington Ave. Manchester, Kentucky 39495",
  },
  {
    title: "California office",
    location: "California",
    direction: "3891 Ranchview Dr. Richardson, California 62639",
  },
  {
    title: "Office schedule",
    location: "Working hours",
    direction: "Monday to Friday 9 am to 10pm",
  },
];

export const optionSubMenus = [
  "Abous",
  "Reservation",
  "Chefs",
  "Testimonial",
  "FAQ",
  "User Pages",
  "Customer Dashboard",
  "Privacy Policy",
  "Terms Of Service",
  "404 Error",
];

export const foodSubMenu = [
  "Food Grid",
  "Food Detais",
  "Card View",
  "Checkout View",
  "Order Success",
];

export const blogSubMenu = ["Blog", "Blog Details"];

export const opcionMenu = [
  "Home",
  "About us",
  "Foood Menu",
  "Blog",
  "Pages",
  "Contact",
];
