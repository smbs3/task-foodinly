import React from 'react';

const Index = ({title, text, children}) => {
    return (
        <section className="Section">
        <p className="Section-heading">{title}</p>
        <h3 className="Section-title">{text}</h3>
        {children}
        </section>
    );
}

export default Index;
