import React from 'react'

const Icon = ({ className, onClick }) => {
  return <i className={className} onClick={onClick}/>;
};



export default Icon