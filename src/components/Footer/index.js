import React from "react";
import Image from "../Image";
import Icon from "../Icon";
const Footer = () => {
  return (
    <footer>
      <div className="Footer">
        <div className="Footer-container">
          <h5 className="Footer-title">Need any help?</h5>
          <ul className="FooterHelp-container">
            <li className="FooterHelp">
              <p className="FooterHelp-heading">Call 24/7 for any help</p>
              <p className="FooterHelp-text">
                <a href="/#" className="FooterHelp-link">+00 123 456 789</a>
              </p>
            </li>
            <li className="FooterHelp">
              <p className="FooterHelp-heading">Mail to our support team</p>
              <p className="FooterHelp-text">
                <a href="/#" className="FooterHelp-link">support@domain.com</a>
              </p>
            </li>
            <li className="FooterHelp">
              <p className="FooterHelp-heading">Follow us on</p>
              <div className="FooterHelp-text">
                <Icon className="fa-brands fa-square-facebook FooterHelp-icon" />
                <Icon className="fa-brands fa-square-twitter FooterHelp-icon" />
                <Icon className="fa-brands fa-instagram FooterHelp-icon" />
                <Icon className="fa-brands fa-linkedin FooterHelp-icon" />
              </div>
            </li>
          </ul>
        </div>

        <div className="Footer-container">
          <h5 className="Footer-title">Quick Links</h5>
          <ul className="Footer-list">
            <li className="Footer-item">
              <a href="/#" className="Footer-link">About US</a>
            </li>
            <li className="Footer-item">
              <a href="/#" className="Footer-link">Testimonials</a>
            </li>
            <li className="Footer-item">
              <a href="/#" className="Footer-link">Faqs</a>
            </li>
            <li className="Footer-item">
              <a href="/#" className="Footer-link">Terms Service</a>
            </li>
            <li className="Footer-item">
              <a href="/#" className="Footer-link">Food Grid</a>
            </li>
            <li className="Footer-item">
              <a href="/#" className="Footer-link">Blog</a>
            </li>
          </ul>
        </div>

        <div className="Footer-container">
          <h5 className="Footer-title">Support</h5>
          <ul className="Footer-list">
            <li className="Footer-item">
              <a href="/#" className="Footer-link">Customer Support</a>
            </li>
            <li className="Footer-item">
              <a href="/#" className="Footer-link">Login</a>
            </li>
            <li className="Footer-item">
              <a href="/#" className="Footer-link">Cart</a>
            </li>
            <li className="Footer-item">
              <a href="/#" className="Footer-link">Contact</a>
            </li>
            <li className="Footer-item">
              <a href="/#" className="Footer-link">Reservation</a>
            </li>
            <li className="Footer-item">
              <a href="/#" className="Footer-link">Privacy Policy</a>
            </li>
          </ul>
        </div>

        <div className="Footer-container">
          <h5 className="Footer-title">Opening hours</h5>
          <table className="FooterTable">
            <tbody>
              <tr>
                <td className="FooterTable-item">Monday</td>
                <td className="FooterTable-item">0.9.00 - 18.00</td>
              </tr>
              <tr>
                <td className="FooterTable-item">Tuesday</td>
                <td className="FooterTable-item">10.00 - 18.00</td>
              </tr>
              <tr>
                <td className="FooterTable-item">Wednesday</td>
                <td className="FooterTable-item">11.00 - 18.00</td>
              </tr>
              <tr>
                <td className="FooterTable-item">Thrusday</td>
                <td className="FooterTable-item">12.00 - 18.00</td>
              </tr>
              <tr>
                <td className="FooterTable-item">Friday</td>
                <td className="FooterTable-item">14.00 - 18.00</td>
              </tr>
              <tr>
                <td className="FooterTable-item">Saturday, Sunday</td>
                <td className="FooterTable-item">Closed</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div className="FooterCopyright">
        <div className="FooterCopyright-container">
          <p className="FooterCopyright-text">
            Copyright © 2022 All Rights Reserved
          </p>
          <Image
            src="https://foodingly.netlify.app/assets/img/common/cards.png"
            className="FooterCopyright-image"
          />
        </div>
      </div>
    </footer>
  );
};

export default Footer;
