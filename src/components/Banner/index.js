import React from "react";
import Icon from "../Icon";

const Banner = ({ namePage }) => {
  return (
    <div className="Banner">
      <h2>{namePage}</h2>
      <ul className="Banner-menu">
        <li className="Banner-link">Inicio</li>
        <li className="Banner-link"><Icon className="fas fa-circle icon"></Icon>{namePage}</li>
      </ul>
    </div>
  );
};

export default Banner;
