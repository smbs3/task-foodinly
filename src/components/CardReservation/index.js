import React from 'react';
import Image from "../Image";

const CardReservation = ({products}) => {
    return (
        <div className="OurReservationCard">
        <Image className="OurReservationCard-icon" src={products.icono} />
        <div className="OurReservationCard-content">
          <a href="/#" className="OurReservationCard-title">
            {products.title}
          </a>
          <p className="OurReservationCard-text">{products.text}</p>
          <Image src={products.image} className="OurReservationCard-imagen" />
        </div>
      </div>
    );
}

export default CardReservation;
