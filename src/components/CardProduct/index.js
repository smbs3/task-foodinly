import React, {useContext, Fragment} from 'react';
import Image from "../Image";
import Link from "../Link";
import Button from "../Button";
import { FoodContext } from "../../context/FoodContext";

const CardProduct = ({product}) => {
  const context = useContext(FoodContext);
  const productoDiscount = (discount) => {
    if (!discount) return <Fragment />;

    return <div className="ProductCard-discount">{discount}%</div>;
  };

    return (
        <div className="ProductCard" key={product.name}>
        <div className="ProductCard-header">
          <Image className="ProductCard-image" src={product.image} />
          {productoDiscount(product.discount)}
        </div>
        <div className="ProductCard-content">
          <h3 className="ProductCard-title">
            <Link className="ProductCard-link" href="#">
              {product.name}
            </Link>
          </h3>
          <p className="ProductCard-text">
            <span className="ProductCard-rating">4.8/5 Excellent</span>
            <span className="ProductCard-review">(1214 reviews)</span>
          </p>
        </div>
        <div className="ProductCard-footer">
          <p className="ProductCard-price">$ {product.price}</p>
          <Button
            className="ProductCard-button"
            onClick={() => context?.addProduct(product.id)}
          >
            Add to Cart
          </Button>
        </div>
      </div>
    )
}

export default CardProduct;
