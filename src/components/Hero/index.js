import React from 'react'
import Link from '../Link'

const Hero = () => {
  return (
    <div>
        <h3><span>Contact</span></h3>
        <div>
            <Link>Home</Link>
            <Link>Contact</Link>
        </div>
    </div>
  )
}

export default Hero