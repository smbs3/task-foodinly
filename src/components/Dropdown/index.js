import React, { useState } from "react";
import Dropdown from "react-bootstrap/Dropdown";


const DropdownBar = ({ arrays = [], toggleClass, itemClass, defaults, align, menuHover }) => {
  const [value, setValue] = useState(defaults);

  const handleSelect = (array) => () => {
    setValue(array);
  };
  const renderItem = () => {
    return arrays.map((array) => (
      <Dropdown.Item
        className={itemClass}
        onClick={handleSelect(array)}
        key={array}
      >
        {array}
      </Dropdown.Item>
    ));
  };

  return (
    <Dropdown align={align}>
      <Dropdown.Toggle className={toggleClass}>{value}</Dropdown.Toggle>
      <Dropdown.Menu className={menuHover}>{renderItem()}</Dropdown.Menu>
    </Dropdown>
  );
};

export default DropdownBar;
