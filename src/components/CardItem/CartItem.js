import React from "react";
import Icon from "../Icon";
import Image from "../Image";

const Cart = ({ product }) => {
  return (
    <div className="CartProduct">
      <div className="CartProduct-info">
        <Image src={product.image} className="Cart-image" />
        <div className="Cart-content">
          <p className="Cart-name">{product.name}</p>
          <span className="Cart-price">1 x ${product.price}</span>
        </div>
      </div>
      <Icon className="fa-solid fa-trash-can Cart-delete" />
    </div>
  );
};

export default Cart;
