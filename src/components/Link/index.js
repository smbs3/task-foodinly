import React from 'react'

const Link = ({children, className, href}) => {
  return (
    <a className={className} href={href}>{children}</a>
  )
}

export default Link;