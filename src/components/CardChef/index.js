import React from 'react';
import Image from "../Image";
import Icon from "../Icon";
import Link from "../Link";


const CardChef = ({chef}) => {
    return (
        <div className="ChefsCard">
        <Image src={chef.image} />
        <div className="ChefsCard-content">
          <ul className="ChefsCard-socialmedia">
            <li className="ChefsCard-icon">
              <Link>
                <Icon className="fa-brands fa-facebook-f" />
              </Link>
            </li>
            <li className="ChefsCard-icon">
              <Link>
                <Icon className="fa-brands fa-twitter" />
              </Link>
            </li>
            <li className="ChefsCard-icon">
              <Link>
                <Icon className="fa-brands fa-instagram" />
              </Link>
            </li>
            <li className="ChefsCard-icon">
              <Link>
                <Icon className="fa-brands fa-linkedin-in" />
              </Link>
            </li>
          </ul>
          <h4 className="ChefsCard-name">
            <span>{chef.name}</span>
          </h4>
          <p className="ChefsCard-charge">{chef.position}</p>
        </div>
      </div>
    );
}

export default CardChef;
