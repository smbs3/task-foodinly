import React from "react";
import Carousel, {
  autoplayPlugin,
  slidesToShowPlugin,
} from "@brainhubeu/react-carousel";
import "@brainhubeu/react-carousel/lib/style.css";

const CustomCarousel = ({ num,children, className }) => {
  return (
    <Carousel 
     className={className}
      plugins={[
        "infinite",
        {
          resolve: autoplayPlugin,
          options: {
            interval: 4000,
          },
        },
        {
          resolve: slidesToShowPlugin,
          options: {
            numberOfSlides: num,
          },
        },
      ]}
      breakpoints={{
        992: {
          plugins: [
            "infinite",
            {
              resolve: autoplayPlugin,
              options: {
                interval: 5000,
              },
            },
            {
              resolve: slidesToShowPlugin,
              options: {
                numberOfSlides: 2,
              },
            },
          ],
        },
        576: {
          plugins: [
            "infinite",
            {
              resolve: autoplayPlugin,
              options: {
                interval: 5000,
              },
            },
            {
              resolve: slidesToShowPlugin,
              options: {
                numberOfSlides: 1,
              },
            },
          ],
        },
      }}
      animationSpeed={2000}
    >
      {children}
    </Carousel>
  );
};

export default CustomCarousel;
