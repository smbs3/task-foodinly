import React from 'react';
import Icon from "../Icon";
import Image from "../Image";

const CardTestimonial = ({opinion}) => {
    return (
        <div className="TestimonialsCard" key={opinion.name}>
        <div className="TestimonialsCard-header">
          <span>{opinion.date}</span>
          <Image className="TestimonialsCard-image" src={opinion.image} />
          <div className="TestimonialsCard-review">
            <span>Excellent</span>
            <div className="TestimonialsCard-rating">
              <Icon className="fa-solid fa-star TestimonialsCard-icon" />
              <Icon className="fa-solid fa-star TestimonialsCard-icon" />
              <Icon className="fa-solid fa-star TestimonialsCard-icon" />
              <Icon className="fa-solid fa-star TestimonialsCard-icon" />
              <Icon className="fa-solid fa-star TestimonialsCard-icon" />
            </div>
          </div>
        </div>
        <div className="TestimonialsCard-content">
          <h3 className="TestimonialsCard-title">
            <span>{opinion.name}</span>
          </h3>
          <p className="TestimonialsCard-text">
            “Eu ipsum ut dolore magna minim cupidatat ullamco anim sit minim
            irure. Consectetur voluptate nisi magna consectetur cillum proident
            dolore veniam voluptate adipisicing labore. Anim eiusmod dolor quis
            pariatur.”
          </p>
        </div>
      </div>
    );
}

export default CardTestimonial;
