import React from 'react';
import Header from '../Header/Header';
import Bar from '../Header/Bar';
import Footer from '../Footer';
import BarContact from '../Header/BarContact';
import MenuMobile from '../Header/MenuMobile';

const Layout = () => {
    return (
        <>
      <Header>
        <MenuMobile />
        <BarContact />
        <Bar />
      </Header>
      <Footer/>
    </>
    );
}

export default Layout;
