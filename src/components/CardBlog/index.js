import React from "react";
import Icon from "../Icon";

const CardBlog = ({ blog }) => {
  return (
    <div className="BlogCard">
      <div className="BlogCard-containerImage">
        <div
          className="BlogCard-image"
          style={{
            backgroundImage: `url(${blog.image})`,
          }}
        ></div>
        <span className="BlogCard-category">{blog.category}</span>
      </div>
      <div className="BlogCard-content">
        <h3 className="BlogCard-title">{blog.title}</h3>
        <p className="BlogCard-text">{blog.text}</p>
      </div>
      <div className="BlogCardFooter">
        <div className="BlogCardFooter-information">
          <Icon className="fa-solid fa-calendar-days BlogCardFooter-icon" />
          <div>
            <span className="BlogCardFooter-label">Date:</span>
            <p className="BlogCard-author">{blog.date}</p>
          </div>
        </div>
        <div className="BlogCardFooter-information Line">
          <Icon className="fa-solid fa-users BlogCardFooter-icon" />
          <div>
            <span className="BlogCardFooter-label">By:</span>
            <p className="BlogCardFooter-author">{blog.by}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardBlog;
