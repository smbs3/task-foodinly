import React from 'react';
import Button from '../Button';

const SpecialOffers = () => {
  return (
    <section className='SpecialOffers'>
        <p className='SpecialOffers-heading'>Special Offers</p>
        <h2 className='SpecialOffers-title'>Preferred Food, Drinks, Juice 30% Off Friday Only</h2>
        <Button className='button'>Buy Now</Button>
    </section>
  )
}

export default SpecialOffers