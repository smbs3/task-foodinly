import React, { useState } from "react";
import Button from "../Button";
import Card from "../CardProduct";
import useAxios from "axios-hooks";
import Section from "../Section";
import { v4 as uuidv4 } from 'uuid';

const Products = () => {

  const [active, setActive] = useState("");

  const [{ data }, refetch] = useAxios("/products");

  const filterList = async (category) => {
    await refetch({
      params: {
        category,
      },
    });
    setActive(category);
  };


  const renderProducts = () => {
    return data?.map((product) => (
      <Card
      key={uuidv4()}
        product={product}
      ></Card>
    ));
  };

  const getIsActive = (label) => {
    if (active === label) return "active";
    return "";
  };

  return (
    <Section title="Our Featured Items" text="Our most popular items">
      <div className="Product-categories">
        <Button
          className={`Product-tab ${getIsActive("")}`}
          onClick={() => filterList("")}
        >
          All Categories
        </Button>
        <Button
          className={`Product-tab ${getIsActive("NOODLES")}`}
          onClick={() => filterList("NOODLES")}
        >
          Noodles
        </Button>
        <Button
          className={`Product-tab ${getIsActive("BURGER")}`}
          onClick={() => filterList("BURGER")}
        >
          Burger
        </Button>
        <Button
          className={`Product-tab ${getIsActive("CHICKEN")}`}
          onClick={() => filterList("CHICKEN")}
        >
          Chicken
        </Button>
        <Button
          className={`Product-tab ${getIsActive("ICE_CREAM")}`}
          onClick={() => filterList("ICE_CREAM")}
        >
          Ice Cream
        </Button>
        <Button
          className={`Product-tab ${getIsActive("DRINKS")}`}
          onClick={() => filterList("DRINKS")}
        >
          Drinks
        </Button>
      </div>
      <div className="Product-item">{renderProducts()}</div>
    </Section>
  );
};

export default Products;
