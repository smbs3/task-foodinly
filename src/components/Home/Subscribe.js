import React from "react";
import Button from "../Button";
import Image from "../Image";

const Subscribe = () => {
  return (
    <section className="Subscribe">
      <div className="Subscribe-container">
        <div className="Subscribe-content">
          <Image
            src="https://foodingly.netlify.app/assets/img/common/email.png"
            className="Subscribe-image"
          />
          <div className="Subscribe-text">
            <p className="Subscribe-heading">Get the latest Blogs and offers</p>
            <h4 className="Subscribe-title">Subscribe to our Blogsletter</h4>
          </div>
        </div>
        <div className="Subscribe-form">
          <input
            type="text"
            className="Subscribe-input"
            placeholder="Enter your mail address"
          />
          <Button className="Subscribe-button">Subscribe</Button>
        </div>
      </div>
    </section>
  );
};

export default Subscribe;
