import React from "react";
import { blogItem } from "../../utils/data";
import { v4 as uuidv4 } from 'uuid';
import CardBlog from "../CardBlog";
import Section from "../Section";

const Blog = () => {
  const renderBlogs = () => {
    return blogItem.map((blog) => <CardBlog blog={blog} key={uuidv4()}></CardBlog>);
  };
  return (
    <Section title="Our Blog" text="Our Lastest Blogs And Blogs">
      <div className="Blog-container">{renderBlogs()}</div>
    </Section>
  );
};

export default Blog;
