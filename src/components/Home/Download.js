import React from "react";
import Image from "../Image";

const Download = () => {
  return (
    <section className="Download">
      <div className="Download-container">
        <div className="Download-content">
          <h2 className="Download-title">
            Never Feel Hungry! Download Our Mobile App & Enjoy Delicious Food
          </h2>
          <p className="Download-text">
            Ut voluptate cupidatat aute et culpa sit sint occaecat ut dolor
            demon consequat eu in id. Eu ex ea commodo.
          </p>
          <div className="Download-apps">
            <Image
              src="https://foodingly.netlify.app/assets/img/promotion/apple-store-icon.png"
              alt="Available on App Store"
              className="Download-app"
            />
            <Image
              src="https://foodingly.netlify.app/assets/img/promotion/google-play-store-icon.png"
              alt="Get it on google Play"
              className="Download-app"
            />
          </div>
        </div>
        <Image
          src="https://foodingly.netlify.app/assets/img/promotion/app.png"
          alt="two phones displaying the app"
          className="Download-image"
        />
      </div>
    </section>
  );
};

export default Download;
