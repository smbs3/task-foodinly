import React from "react";
import Button from "../Button";
import Image from "../Image";

const Reservation = () => {
  return (
    <section className="Reservation">
      <div className="Reservation-container">
        <div className="Reservation-content">
          <h2 className="Reservation-title">
            Do You Have Any Dinner Plan Today? Reserve Your Table
          </h2>
          <p className="Reservation-text">
            Ut voluptate cupidatat aute et culpa sit sint occaecat ut dolor
            demon consequat eu in id. Eu ex ea commodo.
          </p>
          <Button className="button">Make Reservation</Button>
        </div>
        <Image
          src="https://foodingly.netlify.app/assets/img/promotion/promo-1.png"
          alt="plate ilustration"
          className="Reservation-image"
        />
      </div>
    </section>
  );
};

export default Reservation;
