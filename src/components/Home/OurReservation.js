import React from "react";
import Section from "../Section";
import { v4 as uuidv4 } from 'uuid';
import CustomCarousel from "../CustomCarousel";
import { productInfo } from "../../utils/data";
import CardReservation from "../CardReservation";

const OurReservation = () => {
  const renderProduct = () => {
    return productInfo.map((products) => (
      <CardReservation products={products} key={uuidv4()}></CardReservation>
    ));
  };

  return (
    <Section title="Our Reservation" text="What we Offer">
      <CustomCarousel num={4} className="OurReservation-container">
        {renderProduct()}
      </CustomCarousel>
    </Section>
  );
};

export default OurReservation;
