import React from 'react'
import Image from '../Image'

const Hero = () => {
  return (
    <div className='Hero'>
        <div className='Hero-container'>
            <div>
                <p className='Hero-text'>Best dishes and ingredients</p>
                <h1 className='Hero-title'>The best options of the day in your town</h1>
                <p className='Hero-text'>Aliqua enim amet anim nisi minim amet veniam eu magna tempor laboris. Duis veniam officia culpa sunt deserunt nisi</p>
            </div>
            <Image src='https://foodingly.netlify.app/assets/img/banner/bg1.png' alt='plate with salad' className='Hero-image'/>
        </div>

    </div>
  )
}

export default Hero