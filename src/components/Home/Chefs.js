import React from "react";
import { v4 as uuidv4 } from 'uuid';
import Section from "../Section"
import { ChefInfo } from "../../utils/data";
import CardChef from "../CardChef";

const Chefs = () => {
 

  const renderChefs = () => {
    return ChefInfo.map((chef) => (
    <CardChef chef={chef} key={uuidv4()}></CardChef>
    ));
  };
  return (
    <Section title="Meet Our Chefs" text="Our Experienced chefs">
      <div className="Chefs-container">
      {renderChefs()}
      </div>
    </Section>
     
    
  );
};

export default Chefs;
