import React from "react";
import { v4 as uuidv4 } from 'uuid';
import Section from "../Section"
import { opinions } from "../../utils/data";
import CustomCarousel from "../CustomCarousel"
import CardTestimonial from "../CardTestimonial";

const Testimonials = () => {
  

  const renderOpinions = () => {
    return opinions.map((opinion) => (
     <CardTestimonial opinion={opinion} key={uuidv4()}></CardTestimonial>
    ));
  };

  return (
    <Section title="Testimonials" text="What Our Client's Say About Us">
      <CustomCarousel num={3} className="Testimonials-container">
        {renderOpinions()}
      </CustomCarousel>
    </Section>
  );
};

export default Testimonials;
