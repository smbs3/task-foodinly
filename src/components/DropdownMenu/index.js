import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import Icon from "../Icon";

const DropdownMenu = ({ arrays = [], text, to }) => {
  const renderIcon = () => {
    if (arrays.length) return <Icon className="fa-solid fa-angle-down" />;
    return <Fragment></Fragment>;
  };

  const renderOptions = () => {
    if (!arrays.length) return <Fragment></Fragment>;
    return (
      <ul className="BarSubItem">
        {arrays.map((option) => (
          
          <li className="BarSubItem-li">{option}</li>
        ))}
      </ul>
    );
  };

  const renderText = () => {
    console.log(arrays);
    if (arrays.length) return text;
    return (
      <Link className="Bar-link" to={to}>
        {text}
      </Link>
    );
  };

  return (
    <li className="Bar-item">
      {renderText()} {renderIcon()}
      {renderOptions()}
    </li>
  );
};

export default DropdownMenu;
