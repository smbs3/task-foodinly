import React, { useContext, useEffect, useState, useMemo } from "react";
import { Outlet } from "react-router-dom";
import Icon from "../Icon";
import Button from "../Button";
import { Dock } from "react-dock";
import { FoodContext } from "../../context/FoodContext";
import Cart from "../CardItem/CartItem";
import Dropdown from "../DropdownMenu";
import { blogSubMenu, foodSubMenu, optionSubMenus } from "../../utils/data";


const Bar = () => {
  const [isVisible, setisVisible] = useState(false);
  const [products, setProducts] = useState([]);

  const context = useContext(FoodContext);

  useEffect(() => {
    const fetchData = async () => {
      if (isVisible) {
        const cart = await context.getCart();
        setProducts(cart.data.products);
      }
    };
    fetchData();
  }, [isVisible, context]);

  const handleChange = () => {
    setisVisible(!isVisible);
  };

  const subTotal = useMemo(() => {
    if (!products) return;
    let sum = 0;
    products.forEach((product) => {
      sum = sum + product.price;
    });
    return sum;
  }, [products]);

  return (
    <>
      <nav className="Bar">
        <div className="Bar-options">
          <img
            src="https://foodingly.netlify.app/assets/img/logo.png"
            alt="Foodingly Logo"
          />
          <ul className="Bar-menu">
            <Dropdown text="Home" to="/to" />
            <Dropdown text="About Us" to="/#" />
            <Dropdown text="Food Menu" arrays={foodSubMenu} />
            <Dropdown text="Blog" arrays={blogSubMenu} />
            <Dropdown text="Page" arrays={optionSubMenus} />
            <Dropdown text="Contact" to="/contact" />
          </ul>
        </div>
        <div className="Bar-buttons">
          <Icon
            className="fa-solid fa-bag-shopping Bar-icon"
            onClick={handleChange}
          />
          <span className="Bar-span">{products.length}</span>
          <Dock
            position="right"
            isVisible={isVisible}
            size="0.165"
            fluid="true"
            dimMode="opaque"
            onVisibleChange={handleChange}
          >
            <div className="Cart-visible">
              <div className="Cart-container">
                <div className="Cart-header">
                  <p className="Cart-title">My Cart ({products.length})</p>
                  <Icon className="fa-solid fa-xmark" onClick={handleChange} />
                </div>
                <div className="Cart-Body">
                  {products?.map((product) => (
                    <Cart product={product} />
                  ))}
                </div>
                <div className="Cart-footer">
                  <p className="Cart-subtotal">Subtotal</p>
                  <span>${subTotal}</span>
                </div>
                <Button className="button slim">Checkout</Button>
              </div>
            </div>
          </Dock>

          <Icon className="fa-solid fa-magnifying-glass Bar-icon green" />
          <Button className="button Secondary">Reservation</Button>
        </div>
      </nav>
      <Outlet />
    </>
  );
};

export default Bar;
