import React from 'react';
import { opcionMenu } from '../../utils/data';
import  Dropdown  from '../Dropdown';
import Icon from '../Icon';



const MenuMobile = () => {
 
 const MenuEllipsis = [<Icon className="fa-solid fa-magnifying-glass"/>, <Icon
 className="fa-solid fa-bag-shopping AppBarOptions-icon"/>

]
const MenuIcon = (<Icon className='fa-solid fa-bars'/>)
const ellipsis = (<Icon className='fa-solid fa-ellipsis' />)

  return (
    <div className='barMobile'>
     <div className='barMobile-image'>
     <img
            src="https://foodingly.netlify.app/assets/img/logo.png"
            alt="Foodingly Logo"
          />
     </div>
     <ul className="barMobile-settings">
        <li className="barMobile-list">
          <Dropdown
          align="start"
            arrays={opcionMenu}
            toggleClass="barMobile-toggle"
            itemClass="barMobile-menu"
            defaults={MenuIcon}
          />
        </li>
        <li className="">
        <Dropdown
            arrays={MenuEllipsis}
            toggleClass="barMobile-toggle"
            itemClass="barMobile-item"
            defaults={ellipsis}
          />
        </li>
      </ul>
    </div>
  );
}

export default MenuMobile;
