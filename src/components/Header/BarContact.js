import React from "react";
import Icon from "../Icon";
import Dropdown from "../Dropdown";
const BarContact = () => {

  const lenguaje = ["English", "Arabic", "French"];
  const currency = ["USD", "BD", "URO"];
  

  return (
    <nav className="BarContact">
      <ul className="BarContact-info">
        <li className="BarContact-icons">
          <Icon className="fa-brands fa-square-facebook BarContact-icon" />
          <Icon className="fa-brands fa-square-twitter BarContact-icon" />
          <Icon className="fa-brands fa-instagram BarContact-icon" />
          <Icon className="fa-brands fa-linkedin BarContact-icon" />
        </li>
        <li className="BarContact-item">+011 234 567 89</li>
        <li className="BarContact-item">contact@domain.com</li>
      </ul>
      <ul className="BarContact-settings">
        <li className="BarContact-item">
          <Dropdown
            arrays={lenguaje}
            toggleClass="BarContactDropdown"
            itemClass="BarContactDropdown-item"
            defaults="English"
          />
        </li>
        <li className="BarContact-item">
        <Dropdown
            arrays={currency}
            toggleClass="BarContactDropdown"
            itemClass="BarContactDropdown-item"
            defaults="USD"
          />
        </li>
        <li className="BarContact-item item">My Account</li>
      </ul>
    </nav>
  );
};

export default BarContact;
