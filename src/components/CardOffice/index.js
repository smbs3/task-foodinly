import React from 'react';
import Link from "../Link";

const CardOffice = ({office}) => {
    return (
        <div className="OfficeCard" key={office.title}>
        <h6 className="OfficeCard-office">
          <span>{office.title}</span>
        </h6>
        <h3 className="OfficeCard-title">
          <span>{office.location}</span>
        </h3>
        <p>{office.direction}</p>
        <Link className="OfficeCard-link">View on map</Link>
      </div>
    );
}

export default CardOffice;
