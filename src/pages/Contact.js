import React from "react";
import { useFormik } from "formik";
import Banner from "../components/Banner";
import { offices } from "../utils/data";
import CardOffice from "../components/CardOffice";
import { v4 as uuidv4 } from 'uuid';

const Contact = () => {
  const renderOffices = () => {
    return offices.map((office) => <CardOffice office={office} key={uuidv4()}></CardOffice>);
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      phone: "",
      textarea: "",
    },
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });
  return (
    <div className="Contact">
      <Banner namePage="Contact" />
      <div className="Contact-container">
        <h3 className="Contact-title">
          <span>Contact with us</span>
        </h3>
        <div>
          <div className="Contact-heading">
            <p className="Contact-label">Stay in touch</p>
            <p className="Contact-phone">+00 123 456 789</p>
          </div>
          <div className="Office">{renderOffices()}</div>
        </div>
        <h3 className="Contact-title">
          <span>Leave us a message</span>
        </h3>
        <form onSubmit={formik.handleSubmit} className="Form">
          <input
            id="name"
            name="name"
            type="name"
            className="Form-input"
            onChange={formik.handleChange}
            value={formik.values.name}
            placeholder="First Name"
          />
          <input
            id="email"
            name="email"
            type="email"
            className="Form-input"
            onChange={formik.handleChange}
            value={formik.values.email}
            placeholder="Email Adress"
          />
          <input
            id="phone"
            name="phone"
            type="phone"
            className="Form-input"
            onChange={formik.handleChange}
            value={formik.values.phone}
            placeholder="Phone"
          />
          <textarea
            id="textarea"
            name="textarea"
            type="textarea"
            className="Form-input textArea"
            onChange={formik.handleChange}
            value={formik.values.textarea}
            placeholder="Write Message"
          />
          <button type="submit" className="button Form-button">
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default Contact;
