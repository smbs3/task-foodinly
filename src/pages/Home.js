import React from "react";
import Blog from "../components/Home/Blog";
import Chefs from "../components/Home/Chefs";
import Download from "../components/Home/Download";
import Hero from "../components/Home/Hero";
import OurReservation from "../components/Home/OurReservation";
import Product from "../components/Home/Product";
import Reservation from "../components/Home/Reservation";
import SpecialOffers from "../components/Home/SpecialOffers";
import Subscribe from "../components/Home/Subscribe";
import Testimonials from "../components/Home/Testimonials";

const Home = () => {
  return (
    <main>
      <Hero />
      <OurReservation />
      <SpecialOffers />
      <Product />
      <Reservation />
      <Chefs />
      <Download />
      <Testimonials />
      <Subscribe />
      <Blog />
    </main>
  );
};

export default Home;
